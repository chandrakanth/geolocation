// Wait for Cordova to load
var latitude;
var longitude;
var accuracy;
var latlng;
var speed;
var timestamp;
var loc;
var map;


function read()
{
        try
        {
                document.addEventListener("deviceready", onDeviceReady, false);                         
        }
        
        catch(error1)
        {
                alert(error1);
        }       
}
                
// Cordova is ready
                
function onDeviceReady() 
{
        try
        {               
                        //get position from the device  
                        navigator.geolocation.getCurrentPosition(myPositionSuccess, myPositionError, {frequency:5000, maximumAge: 30000, timeout:10000, enableHighAccuracy: true});
                        
                        //watch for change in position
                        navigator.geolocation.watchPosition(myWatchSuccess,myPositionError,{frequency:5000})
        }
        
        catch(error2)
        {
                alert(error2);
        }       
        
}
        
        
// on getting the geolocation

function myPositionSuccess(position) 
{
        try
        {
                //get latitude,longitude,accuracy etc values
                latitude = position.coords.latitude;
                longitude = position.coords.longitude;
                accuracy = position.coords.accuracy;
                speed = position.coords.speed;
                timestamp = position.timestamp;
                
                displayLocation();
                //sendLocation();
                mapping();
        }
        
        catch(error3) 
        {
                alert(error3);
        }
}

//each time the position changes
function myWatchSuccess(position) 
{
        try
        {
                //get latitude,longitude,accuracy etc values
                latitude = position.coords.latitude;
                longitude = position.coords.longitude;
                accuracy = position.coords.accuracy;
                speed = position.coords.speed;
                timestamp = position.timestamp;
                
                displayLocation();
                //sendLocation();
                //mapping();
        }
                
        catch(error4) 
        {
                alert(error4);
        }
        
}

function displayLocation()
{
        var element = document.getElementById('geolocation');
        element.innerHTML = 'Latitude: '           + latitude              + '<br />' +
                        'Longitude: '          + longitude             + '<br />' +
                      //'Altitude: '           + altitude              + '<br />' +
                        'Accuracy: '           + accuracy              + '<br />' +
                      //'Heading: '            + heading               + '<br />' +
                        'Speed: '              + speed                 + '<br />' +
                        'Timestamp: '          + timestamp                    + '<br />';
}       
        
/*function sendLocation()
{
        var locationData = JSON.stringify({
                                           latitude   : latitude,
                                           longitude  : longitude,
                                           accuracy   : accuracy
                                           });
                
        $.ajax({
        type: "POST",
        url: <some url>,
        contentType: "application/json; charset=utf-8",
        data: locationData,
        dataType: 'json',
        timeout: 10000,
        async: true,
        cache: false,               
        headers: { "cache-control": "no-cache" },
        success: function(data, status, xhr){
        alert("Data Successfully sent =" + data.d.Status + data.d.Message);     
        }       
        error: function(xhr, status, error){
        alert("Error Occurred" + status);
                }
        });
}       */
        
// onError Callback receives a PositionError object
function myPositionError(error) 
{
        alert('code: '    + error.code    + '\n' +    'message: ' + error.message + '\n');
}

function mapping() {
        try
        {
                map = L.map('map').setView([latitude, longitude], 13);
                L.tileLayer('http://iridium.purplebeak.com/osm/{z}/{x}/{y}.png', {
                    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a></a>',
                    maxZoom: 18
                }).addTo(map);
                map.locate({setView: true, watch: true, maxZoom: 13});
                loc = L.marker([latitude, longitude]).addTo(map);
                map.on('locationfound', onLocationFound);
                map.on('locationerror', onLocationError);
        }
        
        catch(error4)
        {
                alert(error4)
        }
}

function onLocationFound(e) 
{
        try
        {
                var radius = e.accuracy / 2;
                //alert("(Latitude, Longitude) = " + e.latlng );
                loc.setLatLng([latitude, longitude]).update()
                .bindPopup("You are within " + radius + " meters from this point").openPopup();
                L.circle(e.latlng, radius).addTo(map);
        }
        
        catch(error5)
        {
                alert(error5)
        }
}

function onLocationError(e) 
{
        alert(e.message);
}
