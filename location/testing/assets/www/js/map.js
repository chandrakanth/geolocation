function mapping() {
	try
	{
		var map = L.map('map').setView([latitude, longitude], 13);
		L.tileLayer('http://iridium.purplebeak.com/osm/{z}/{x}/{y}.png', {
		    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a></a>',
		    maxZoom: 18
		}).addTo(map);
		map.locate({setView: true, watch: true, maxZoom: 13});
		L.marker([latitude, longitude]).addTo(map);
		map.on('locationfound', onLocationFound);
		map.on('locationerror', onLocationError);
	}
	
	catch(error4)
	{
		alert(error4)
	}
}

function onLocationFound(e) {
	try
	{
		var radius = e.accuracy / 2;
		alert("Radius = " + radius );
		L.marker(e.latlng).addTo(map)
		.bindPopup("You are within meters from this point").openPopup();
		L.circle([latitude, longitude], radius).addTo(map);
	}
	
	catch(error5)
	{
		alert(error5)
	}
}

function onLocationError(e) {
	alert(e.message);
}


